#!/usr/bin/env python3

import argparse
import importlib
import ipaddress
import json
import re
import socket
import subprocess
import sys
import threading
import time
from abc import abstractmethod
from dataclasses import dataclass
from enum import Enum
from pathlib import Path
from typing import Any, Literal, Optional, Type, Union

import netifaces
from termcolor import colored, cprint

# Regex to parse lines like this:
# 64 bytes from zuvcmslb.rrze.uni-erlangen.de (2001:638:a000:1080::209): icmp_seq=2 ttl=57 time=15.8 ms
# 64 bytes from 2001:638:a000:1080::209: icmp_seq=2 ttl=57 time=15.8 ms
# 64 bytes from 131.188.16.209: icmp_seq=2 ttl=57 time=15.8 ms
PING_RE = re.compile(
    r"(?P<size>\d+) bytes from "
    r"((?P<host>[\w\d\.\-]+) \((?P<ip1>[\da-f\:\.]+)\)|(?P<ip2>[\da-f\:\.]+)): "
    r"icmp_seq=\d+ "
    r"ttl=(?P<ttl>\d+) "
    r"time=(?P<rtt>[\d\.]+) ms"
)


IPVersion = Union[Literal[4], Literal[6]]

MAX_LOCATION_AGE = 5 * 60


def resolve(hostname: str, version: IPVersion) -> Optional[str]:
    flag = {
        4: socket.AF_INET,
        6: socket.AF_INET6,
    }[version]
    results = set[str]()

    for result in socket.getaddrinfo(hostname, None, flag):
        if result[0] != flag:
            continue
        results.add(result[4][0])

    return results.pop() if results else None


def ping(hostname: str, version: IPVersion) -> Optional[float]:
    host_addr = resolve(hostname, version)

    if not host_addr:
        return None
    try:
        output = subprocess.check_output(
            ["ping", "-c", "1", str(host_addr)],
            text=True,
        )
    except subprocess.CalledProcessError as err:
        cprint(str(err), color="grey", file=sys.stderr)

        return None

    for line in output.splitlines():
        match = PING_RE.match(line)

        if not match:
            continue

        cprint(line, color="grey")

        rtt = float(match.group("rtt"))

        return rtt

    assert False

def run_termux_cmd(cmd: list[str], timeout: Optional[int]) -> Optional[dict]:
    """Run a termux command and return the json data."""
    try:
        output = subprocess.check_output(cmd, text=True, timeout=timeout)
    except (subprocess.CalledProcessError, subprocess.TimeoutExpired) as err:
        cprint(str(err), file=sys.stderr, color="red")

        return None
    try:
        return json.loads(output)
    except json.JSONDecodeError as err:
        cprint(str(err), file=sys.stderr, color="red")

        return None


@dataclass
class Location:
    latitude: Optional[float]
    longitude: Optional[float]
    altitude: Optional[float]
    speed: Optional[float]

    @classmethod
    def unknown(cls) -> "Location":
        """docstring for unknown"""

        return Location(
            latitude=None,
            longitude=None,
            altitude=None,
            speed=None,
        )


class LocationBackend:
    def __init__(self, interval: int):
        self.interval = interval
        self.slow_thread = threading.Thread(target=self._slow_thread_func, daemon=True)
        self.fast_thread = threading.Thread(target=self._fast_thread_func, daemon=True)
        self._slow_location: Optional[Location] = None
        self._slow_location_update: Optional[float] = None
        self._fast_location: Optional[Location] = None
        self._fast_location_update: Optional[float] = None
        self._stop = False

    def __enter__(self):
        cprint(f"Using {self.__class__.__name__}", color="cyan")
        cprint("Trying to get fast location...", color="grey")
        self._current_location = self.get_location_fast() or Location.unknown()
        self.slow_thread.start()
        self.fast_thread.start()

        return self

    def __exit__(self, *args, **kwargs):
        cprint("\rStop logging", color="white", attrs=["bold"])
        self._stop = True
        cprint("Waiting for fast thread to join.", color="grey")
        self.fast_thread.join()
        cprint("Waiting for slow thread to join.", color="grey")
        self.slow_thread.join()
        cprint("Location backend exited.", color="grey")

    @property
    def current_location(self) -> Location:
        if (
            self._slow_location
            and self._slow_location_update
            and time.time() - self._slow_location_update < MAX_LOCATION_AGE
        ):
            return self._slow_location
        elif (
            self._fast_location
            and self._fast_location_update
            and time.time() - self._fast_location_update < MAX_LOCATION_AGE
        ):
            return self._fast_location
        else:
            return Location.unknown()

    def _fast_thread_func(self):
        while not self._stop:
            start = time.time()

            cprint("Trying to get fast location...", color="grey")
            loc = self.get_location_fast()

            if not loc:
                cprint("Got no fast location.", color="grey")
            else:
                cprint("Got fast location.", color="grey")
                self._fast_location = loc
                self._fast_location_update = time.time()

            stop = time.time()
            sleep_time = max(0, self.interval - (stop - start))

            if sleep_time and not self._stop:
                cprint(f"Fast location thread will sleep {sleep_time:.3f} s", color="grey")
                time.sleep(sleep_time)

    def _slow_thread_func(self):
        while not self._stop:
            start = time.time()

            cprint("Trying to get slow location...", color="grey")
            loc = self.get_location_slow(timeout=self.interval)

            if not loc:
                cprint("Got no slow location.", color="grey")
            else:
                cprint("Got slow location.", color="grey")
                self._slow_location = loc
                self._slow_location_update = time.time()

            stop = time.time()
            sleep_time = max(0, self.interval - (stop - start))

            if sleep_time and not self._stop:
                cprint(f"Slow location thread will sleep {sleep_time:.3f} s", color="grey")
                time.sleep(sleep_time)

    @abstractmethod
    def get_location_slow(self, timeout: int) -> Optional[Location]:
        """Get a location with up to ``timeout`` delay."""

    @abstractmethod
    def get_location_fast(self) -> Optional[Location]:
        """Get a location without delay."""


class GPSDLocationBackend(LocationBackend):
    def __init__(self, interval: int):
        super().__init__(interval)
        self.gpsd: Any = importlib.import_module("gpsd")

    def __enter__(self):
        self.gpsd.connect()

        return super().__enter__()

    def get_location_fast(self) -> Optional[Location]:
        try:
            packet = self.gpsd.get_current()

            return Location(
                latitude=packet.lat,
                longitude=packet.lon,
                altitude=packet.alt,
                speed=packet.speed(),
                #  sats = packet.sats
                #  sats_valid = packet.sats_valid
            )
        except (self.gpsd.NoFixError, UserWarning) as err:
            cprint(str(err), file=sys.stderr, color="red")

            return None

    def get_location_slow(self, timeout: int) -> Optional[Location]:
        return self.get_location_fast()


class TermuxLocationBackend(LocationBackend):
    def _run_termux_location(
        self, request: Optional[str] = None, timeout: Optional[int] = None
    ) -> Optional[Location]:
        cmd = ["termux-location"]

        if request:
            cmd.extend(["-r", request])

        data = run_termux_cmd(cmd, timeout)

        if not data:
            return None

        return Location(
            latitude=data.get("latitude"),
            longitude=data.get("longitude"),
            altitude=data.get("altitude"),
            speed=data.get("speed"),
        )

    def get_location_fast(self) -> Optional[Location]:
        return self._run_termux_location(request="last")

    def get_location_slow(self, timeout: int) -> Optional[Location]:
        return self._run_termux_location(timeout=timeout)


class FakeLocationBackend(LocationBackend):
    def get_location_fast(self) -> Optional[Location]:
        return Location.unknown()

    def get_location_slow(self, timeout: int) -> Optional[Location]:
        return Location.unknown()


class WiFiBackend:
    @abstractmethod
    def get_wifi_info(self) -> tuple[Optional[float], Optional[str]]:
        """Return SSID and RSSI of current WiFi connection."""


class TermuxWiFiBackend:
    def get_wifi_info(self):
        info = run_termux_cmd(["termux-wifi-connectioninfo"], timeout=5)

        if not info:
            return (None, None)

        return (
            int(info["rssi"]) if "rssi" in info else None,
            info.get("ssid"),
        )


class LinuxWiFiBackend:
    SSID_RE = re.compile(r'\s+SSID: (?P<ssid>.*)')
    RSSI_RE = re.compile(r'\s+signal: (?P<rssi>-?[0-9\.]+) dBm')

    def get_wifi_info(self):
        wifi_interface: Optional[str] = None
        rssi: Optional[float] = None
        ssid: Optional[str] = None

        for _peer_addr, interface in netifaces.gateways()['default'].values():
            if interface.startswith("w"):
                wifi_interface = interface

                break

        if not wifi_interface:
            cprint("Did not find any wifi interface")

        output = subprocess.check_output(["iw", "dev", wifi_interface, "link"], text=True, timeout=5)

        for line in output.splitlines():
            ssid_match = self.SSID_RE.match(line)

            if ssid_match:
                ssid = ssid_match.group('ssid')
            rssi_match = self.RSSI_RE.match(line)

            if rssi_match:
                rssi = float(rssi_match.group('rssi'))

        return (rssi, ssid)


class Env(Enum):
    LINUX = "linux"
    TERMUX = "termux"


LOCATION_BACKENDS = {
    Env.LINUX: GPSDLocationBackend,
    #  Env.LINUX: FakeLocationBackend,
    Env.TERMUX: TermuxLocationBackend,
}

WIFI_BACKENDS = {
    Env.LINUX: LinuxWiFiBackend,
    Env.TERMUX: TermuxWiFiBackend,
}


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "output",
        default="log.csv",
        type=Path,
    )
    parser.add_argument(
        "--env",
        choices=Env,
        type=Env,
        default=Env.TERMUX,
    )
    parser.add_argument(
        "--interval",
        default=60,
        type=int,
    )
    parser.add_argument(
        "--host",
        default="fau.de",
        type=str,
    )

    return parser.parse_args()


def main():
    """docstring for main"""
    args = parse_args()

    location_backend_cls: Type[LocationBackend] = LOCATION_BACKENDS[args.env]
    wifi_backend: WiFiBackend = WIFI_BACKENDS[args.env]()

    if not args.output.is_file():
        with args.output.open("w") as file:
            line_str = f"time;lat;lon;alt;speed;rtt_v4;rtt_v6;rssi;ssid"
            cprint(line_str, attrs=["bold"])
            print(line_str, file=file)

    with location_backend_cls(args.interval) as location_backend:
        with args.output.open("a") as file:
            while True:
                rtt_v6 = ping(args.host, 6)
                rtt_v4 = ping(args.host, 4)
                rssi, ssid = wifi_backend.get_wifi_info()
                print(
                    " ".join(
                        (
                            colored(f"RTT:", attrs=["bold"]),
                            colored(
                                f"IPv4:",
                                attrs=["bold"],
                                color="green" if rtt_v4 else "red",
                            ),
                            colored(
                                f"{rtt_v4:.3f}" if rtt_v4 else "-",
                                attrs=["bold"],
                                color="green" if rtt_v4 else "red",
                            ),
                            colored(
                                f"IPv6:",
                                attrs=["bold"],
                                color="green" if rtt_v6 else "red",
                            ),
                            colored(
                                f"{rtt_v6:.3f}" if rtt_v6 else "-",
                                attrs=["bold"],
                                color="green" if rtt_v6 else "red",
                            ),
                        )
                    )
                )
                location = location_backend.current_location
                line = [
                    int(time.time()),
                    location.latitude or 0,
                    location.longitude or 0,
                    f"{location.altitude:.1f}" if location.altitude else 0,
                    f"{location.speed:.1f}" if location.speed else 0,
                    rtt_v4,
                    rtt_v6,
                    rssi or 0,
                    ssid or "",
                ]
                line_str = ";".join(str(data) for data in line)
                print(line_str, file=file)
                cprint(line_str, attrs=["bold"])
                time.sleep(args.interval)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        sys.exit(0)
